\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage{physics}
\usepackage{amsmath}
\usepackage{pstricks}
\usepackage{graphicx}
\usepackage{qcircuit}
\usepackage{caption}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\newcommand{\conj}[1]{%
  \overline{#1}%
}
\newcommand{\innerprod}[2]{%
  \langle{#1},{#2}\rangle%
}

\title{Shor's Integer Factorization Algorithm}
% \date{\today}
\date{}
\author{David Badke}
\institute{University of Hawaii at Manoa\\ICS 623}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}

\maketitle

\begin{frame}{Table of contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

\section{Introduction}

\begin{frame}[fragile]{Shor's Algorithm}
  Shor's algorithm, discovered by mathematician Peter Shor, is
  a quantum \emph{integer factorization} algorithm that runs in
  polynomial time with respect to the size $\log N$ of the input.\pause

  Shor's Algorithm runs in $O(\log^2 N (\log \log N) (\log \log \log N))$
  time, exponentially faster than the fastest known classical factoring
  algorithm.
\end{frame}

\begin{frame}[fragile]{Shor's Algorithm}
  Shor's algorithm solves the following problem: given a large composite
  integer $N$, find a prime number $d$ that divides $N$.\pause{}

  The assumed difficulty of factoring large integers is the basis for
  many widely used public-key encryption algorithms, such as \emph{RSA}.
\end{frame}

\begin{frame}[fragile]{Shor's Algorithm}
  Shor's algorithm achieves its efficiency by using the
  \emph{Quantum Fourier Transform}, which takes advantage of a phenomenon
  known as \emph{Quantum Parallelism}.
\end{frame}

\begin{frame}[fragile]{Shor's Algorithm}
  Consider a quantum circuit capable of computing a function $f$ by sending
  the computational basis state
  $\ket{x}\ket{y}$ to $\ket{x}\ket{f(x) \oplus y}$.\pause{}

  The by the \emph{principle of superposition}, this same circuit sends a
  \emph{superposition} of basis states to the corresponding superposition of
  $f$ applied to the basis states, effectively evaluating the function
  multiple times in one step.
\end{frame}

\begin{frame}[fragile]{Shor's Algorithm}
  Shor's algorithm is consists of two parts, one of which can be performed
  classically, and is similar to other factoring algorithms.\pause{}

  This step \emph{reduces} the problem to finding the period of the function
  $$x \mapsto a^x \mod N.$$
\end{frame}

\section{Reduction to Period Finding}

\begin{frame}[fragile]{Congruence of Squares}
  One way to factor a composite integer $N$ would be to find integers
  $a \neq b$ such that $a^2 - b^2 = N$. Then $N$ has two nontrivial factors
  $(a + b)$ and $(a - b)$.\pause{}

  In practice, this is inefficient. However, we can weaken this condition to
  requiring that $a^2 \equiv b^2 \pmod{N}$.\pause{}

  In Shor's algorithm, we take $b = 1$, so $a^2 \equiv 1 \pmod{N}$.
  Then $\gcd(a + 1, N)$ and $\gcd(a - 1, N)$ are both non trivial
  factors of $N$.
\end{frame}

\begin{frame}[fragile]{Shor's Algorithm Steps}
  The steps of Shor's Algorithm are\pause{}
  \begin{enumerate}
    \item choose a random odd integer $a < N$.\pause{}
    \item Compute $k = \gcd(a, N)$. If $k$ is not equal to 1,
      then $k$ is a non trivial factor of $N$ and we are done.\pause{}
    \item If not, find the period $r$ of the function
      $x \mapsto a^x \pmod N$. This is the quantum step.\pause{}
    \item If the period $r$ is not even, or if $a^{\frac r 2} \equiv -1 \pmod N$,
      start over with a new $a$.\pause{}
    \item Then $\gcd(a^{\frac r 2} + 1, N)$ and $\gcd(a^{\frac r 2} - 1, N)$
      are two nontrivial factors of $N$.
  \end{enumerate}
\end{frame}

\section{Quantum Period Finding}

{%
  \setbeamercolor{palette primary}{fg=black, bg=yellow}
  \begin{frame}[standout]
    Questions?
  \end{frame}
}%
\appendix

\begin{frame}[allowframebreaks]{References}
  \bibliography{demo}
  \bibliographystyle{abbrv}
\end{frame}

\end{document}
