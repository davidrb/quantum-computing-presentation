\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage{physics}
\usepackage{amsmath}
\usepackage{pstricks}
\usepackage{graphicx}
\usepackage{qcircuit}
\usepackage{caption}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\newcommand{\conj}[1]{%
  \overline{#1}%
}
\newcommand{\innerprod}[2]{%
  \langle{#1},{#2}\rangle%
}

\title{Quantum Computing}
% \date{\today}
\date{}
\author{David Badke}
\institute{University of Hawaii at Manoa\\ICS 623}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}

\maketitle

\begin{frame}{Table of contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

\section{Quantum Primer}

\begin{frame}[fragile]{States}
  \begin{block}{Quantum Mechanics Postulate 1}
    The \alert{state} of a quantum system is represented as a vector in a
    complex \alert{Hilbert space}.
  \end{block}
\end{frame}

\begin{frame}[fragile]{Hilbert Spaces}
  A Hilbert space is
  \begin{itemize}
    \item a \alert{vector space}
    \item with an \alert{inner product}, with respect to which it is complete.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Vector Spaces}
  A \alert{vector space} $(V, +, \times, \mathbb{F})$ over a field $\mathbb{F}$
  is a set along with\pause
  \begin{itemize}
    \item a \emph{vector addition} $+ : V \times V \to V, (u, w)\mapsto u + w$
      \pause
    \item a \emph{scalar multiplication} $\times : \mathbb{F} \times V \to V,
      (k, v)\mapsto kv$ \pause
  \end{itemize}
  That satisfy the vector space axioms.  In Quantum Mechanics, the field is
  always the complex numbers $\mathbb{C}$.
\end{frame}

\begin{frame}[fragile]{Inner Products}
  An \alert{inner product} on a vector space $V$ is a map
  $\langle\cdot,\cdot\rangle : V\times V\to \mathbb{F}$ that is\pause
  \begin{itemize}
    \item conjugate symmetric, $\innerprod v w = \conj{\innerprod w v}$\pause
    \item \emph{linear} in the second argument, $\innerprod v {ku + w} = k
      \innerprod v u + \innerprod v w$\pause \item positive definite,
      $\innerprod{v}{v} \geq 0, = 0$ iff $v = 0$
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Inner Products}
  Inner products generalize the Euclidean dot product to more general vector
  spaces.  They define the \emph{length} of a vector, $\lVert v \rVert =
  \sqrt{\innerprod v v}$ and the \emph{angle} between vectors, $\cos
  \theta_{vw} = \innerprod v w / \lVert v \rVert \lVert w \rVert$.\pause

  A vector is \alert{normalized} if its length is one.  Two vectors are
  \alert{orthogonal} if the angle between them is zero, or equivalently if
  $\innerprod v v = 0$.
\end{frame}

\begin{frame}[fragile]{Basis of a Vector Space}
  A \alert{linear combination} of a set of vectors $\{v, w, \cdots, z\}$ is an
  expression of the form
  $$av + bw + \cdots + cz$$
  \pause
  A set of vectors are \alert{linearly independent} if no vector in the set can
  be written as a linear combination of the others. The \alert{span} of a set
  of vectors is the set of all linear combinations of those vectors.\pause

  A set of vectors that is linearly independent, and which spans the entire
  space, is called a \alert{basis} of that space.
\end{frame}

\begin{frame}[fragile]{Basis of a Vector Space}
  Given a basis $\{e_i\}$, any vector $v$ can be written \emph{uniquely} as a
  linear combination $v_1 e_1 + v_2 e_2 + \cdots + v_n e_n$ of the basis vectors.
  The coefficients $v_i$ are called the \alert{components} of the vector in that basis.

  \pause Any two bases for a vector space have the same number $n$ of elements.
  The number $n$ is called the \alert{dimension} of the space. We will be
  mostly concerned with $2^i$ dimensional spaces.
\end{frame}

\begin{frame}[fragile]{Orthonormal Basis}
  A basis in which every vector is normalized and pairwise orthogonal is called an
  \alert{orthonormal basis}.

  \pause If a basis is orthonormal, the components of a vector $v$ are given by
  $$\innerprod {e_i}{v} = \innerprod{e_i}{\sum v_n e_n} = \sum v_n \innerprod{e_i}{e_n} = v_i$$\pause
  and the action of the inner product between two vectors is given by
  $$\innerprod v w = \innerprod{\sum v_i e_i}{\sum w_j e_j}
  = \sum_{i, j} \conj{v_i} w_j \innerprod{e_i}{e_j} = \sum_{i} \conj{v_i} {w_i}$$
\end{frame}

\begin{frame}[fragile]{dual vector}
  Let $V$ be a vector space. The \alert{dual space} to $V$ is the set of linear
  maps from $V$ to $\mathbb{F}$.

  \pause The inner product gives a natural isomorphism between a vector space
  and its dual, $$v\mapsto\innerprod v \cdot$$
\end{frame}

\begin{frame}[fragile]{bra-ket notation}
  Physicists call a vector $v$ a \alert{ket} and write it $\ket{v}$.  A dual
  vector is called a \alert{bra}, and the dual vector corresponding to $v$ is
  written $\bra v$.

  \pause The inner product between $v$ and $w$ is then written $\bra{v}\ket{w}$.

  \pause This is called \alert{Dirac notation} after P.A.M. Dirac, who invented it.
\end{frame}

\begin{frame}[fragile]{Observables}
  \begin{block}{Quantum Mechanics Postulate 2}
    An \alert{observable} as a \alert{self-adjoint operator} $A$.
    Upon measurement, an observable will be found to have a value equal to one of its
    \alert{eigenvalues}.

    The components of a state $\ket \psi$ expanded in an orthonormal \alert{eigenbasis}
    of $A$ are the \alert{probability amplitudes} of $A$ attaining the eigenvalue
    corresponding to that eigenbasis vector.
  \end{block}
\end{frame}

\begin{frame}[fragile]{Operators}
  An \alert{operator} is a linear map from $V$ to $V$. The action of an operator $A$
  on a vector $\ket{\psi}$ is denoted $A\ket{\psi}$ or $\ket{A\psi}$, and the composition of
  operators $A, B$ is denoted $AB$.

  \pause Examples of operators include \emph{rotations} and \emph{affine transformations}.
\end{frame}

\begin{frame}[fragile]{Matrix representation of Operators}
  Given an orthonormal basis $\{e_i\}$, the action of an operator on $\ket\psi$
  can be written as $$A\ket\psi = A\left(\sum_n\psi_n\ket{e_n}\right) = \sum_n
  \psi_n A \ket{e_n}$$

  \pause So any operator is completely determined by its action on the basis vectors.
  \pause Furthermore,
  \begin{align*}
    \sum_n \psi_n A \ket{e_n}
    &= \sum_{n,m} \psi_n \bra{e_m}A\ket{e_n}\ket{e_m}\\
    &= \sum_{n,m} \psi_n A_{mn} \ket{e_m}\\
  \end{align*}
  Where $A_{mn} = \bra{e_m}A\ket{e_n}$ are the \alert{matrix components} of $A$.
\end{frame}

\begin{frame}[fragile]{Eigenvectors and Eigenvalues}
  Let $A$ be an operator. A non-zero vector $\ket{\psi}$ is an \alert{eigenvector}
  of $A$ if
  $$A\ket{\psi} = \lambda \ket{\psi}$$
  for some value $\lambda$, called the \alert{eigenvalue} of $\ket{\psi}$.
\end{frame}

\begin{frame}[fragile]{Examples of Eigenvectors}
  \begin{table}
    \caption{Example operators and their eigenvectors and eigenvalues.}
    \begin{tabular}{llr}
      \toprule
      Operator & Eigenvector & Eigenvalue\\
      \midrule
      Scale by 2 & any vector & 2\\
      Rotation about x axis & $\ket{i}$ & 1\\
      Projection onto subspace & any $\ket \psi$ in the subspace & 1\\
      \bottomrule
    \end{tabular}
  \end{table}
\end{frame}

\begin{frame}[fragile]{Adjoint of Operators}
  The \alert{adjoint} of an operator A is the operator $A^\dag$ such that
  for all $\phi, \psi$,
  $$\bra{\phi}\ket{A\psi} = \bra{A^\dag \phi}\ket{\psi}$$
  \pause $A$ is called \alert{self adjoint} (or \emph{Hermitian}) if $A = A^\dag$.
\end{frame}

\begin{frame}[fragile]{Eigenvalues of Self Adjoint Operators}
  \begin{theorem}
    The eigenvalues of a self-adjoint operator A are real.
    \pause\begin{proof}
      Let $\ket\psi$ be an eigenvalue of $A$. Then
      \begin{align*}
        \bra\psi A \ket\psi &= \bra\psi \ket{A\psi} = \bra\psi\ket{\lambda\psi} = \lambda\bra{\psi}\ket{\psi}\\
        &= \bra{A^\dag\psi}\ket{\psi} = \bra{A\psi}\ket{\psi} = \bra{\lambda\psi}\ket{\psi} = \conj{\lambda}\bra\psi\ket{\psi}\\
      \end{align*}
      So $\lambda = \conj\lambda$
    \end{proof}
  \end{theorem}
\end{frame}

\begin{frame}[fragile]{Eigenbasis of Self Adjoint Operator}
  Given a self-adjoint operator $A$, we can find a basis consisting of eigenvectors
  of $A$. This is called an eigenbasis of $A$.

  \pause In other words, any state $\ket\psi$ of a quantum system can be written as a
  linear combination (or \alert{superposition}) of the eigenvectors of $A$.
  Then the $i$th component of $\ket\psi$ is the \alert{probability amplitude} of 
  measuring A to have the eigenvalue of the $i$th eigenvector.
\end{frame}

\begin{frame}[fragile]{Eigenbasis of Self Adjoint Operator}
  Note
  \begin{itemize}
    \item
      \pause We require that the state be \emph{normalized} so that the sum of the probabilities
      of measuring the observable equal 1.

    \item
      \pause Multiplying a state by $e^{i \theta}$ does not change the probabilities of
      measuring a given value. So two states that differ by a phase represent the
      same physical state.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Wave Function Collapse}
  \begin{block}{Quantum Mechanics Postulate 3}
    Once a given observable $A$ is measured and found to have a value $\lambda_i$
    corresponding to the eigenvector $\ket{\phi_i}$, the state of the system ``collapses''
    onto to space spanned by $\phi_i$.
    $$\ket{\psi'} = \frac{\bra{\phi_i}\ket{\psi}}{ \abs{\bra{\phi_i}\ket{\psi}}^2}\ket{\phi_i}$$
  \end{block}
\end{frame}

\begin{frame}[fragile]{Example: Spin}
  An important example of a quantum mechanical observable is the \alert{spin}
  of an electron along an axis. The spin of an electron can be measured to be
  either $+1/2$ or $-1/2$, also called ``spin up'' or ``spin down''.\pause

  A physical device that can measure spin is a \alert{Stern-Gerlach apparatus},
  in which an electron is sent through an inhomogenous electromagnetic field,
  and is deflected either up or down, depending on its spin.\pause

  The spin of an electron is an example of a \alert{qubit}, the quantum analog
  of a classical bit.
\end{frame}

\begin{frame}[fragile]{Composit Systems}
  \begin{block}{Quantum Mechanics Postulate 4}
    The Hilbert space of a quantum system consisting of two smaller systems
    with Hilbert spaces $\mathcal{H}_1, \mathcal{H}_2$ is given by the
    \alert{tensor product} $$\mathcal{H} = \mathcal{H}_1 \otimes
    \mathcal{H}_2$$
  \end{block}
\end{frame}

\begin{frame}[fragile]{Tensor Products}
  The tensor product is the freest vector space spanned by vectors of the form
  $v_1 \otimes v_2$, where
  \begin{itemize}
    \item $(v_1 + v_2) \otimes w = v_1 \otimes w + v_2 \otimes w$
    \item $v \otimes (w_1 + w_2) = v_1 \otimes w + v_2 \otimes w$
    \item $a (v \otimes w) = (av) \otimes w = v \otimes (aw)$
  \end{itemize}
  with the inner product $$\innerprod{v_1\otimes w_1}{v_2 \otimes w_2} =
  \innerprod{v_1}{v_2}\innerprod{w_1}{w_2}$$
\end{frame}

\begin{frame}[fragile]{Tensor Products}
  Tensor product properties:
  \begin{itemize}
    \item The zero vector in $\mathcal{H}_1 \otimes \mathcal{H}_2$ can be
      written $0 = 0 \otimes w = v \otimes 0$. \pause

    \item If $\mathcal{H}_1$ and $\mathcal{H}_2$ are $n$ and $m$ dimensional,
      then $\mathcal{H}_1 \otimes \mathcal{H}_2$ is $n \times m$ dimensional.
      (Not $n+m$!) \pause

    \item If $\{e_i\}$ and $\{f_j\}$ are bases for $\mathcal H_1$ and $\mathcal H_2$,
      then $\{e_i \otimes f_j\}$ is a basis for $\mathcal H_1 \otimes \mathcal H_2$. \pause
  \end{itemize}

  Notation:

  The basis vector $\ket i \otimes \ket j$ is usually written just $\ket i \ket j$
  or $\ket {ij}$.
\end{frame}

\begin{frame}[fragile]{Quantum Entanglement}
  Consider a composit system consisting of an $n$-dimensional subsystem and an
  $m$-dimensional subsystem.\pause
  \begin{itemize}
    \item to specify the states of the individual subsystems, you need to
      specify $n + m$ complex numbers.\pause
    \item to specify the state of the entire system, you need to specify $n
      \times m$ complex numbers.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Quantum Entanglement}
  Not every state of the composite system can be written as $v \otimes w$.
  States which cannot be written this way are said to be \alert{entangled}.
\end{frame}

\begin{frame}[fragile]{Bell States}
  Let $\mathcal H$ be a tensor product of two dimensional Hilbert spaces, each
  with basis vectors labelled $\ket 0$ and $\ket 1$. Then $\mathcal H$ is four
  dimensional, and has a basis $\{\ket{00}, \ket{01}, \ket{10},
  \ket{11}\}$.\pause

  We can define a basis of \emph{entangled} states in terms of these.  The
  elements of this basis are known as the \alert{Bell states}.
  $$
  \frac{\ket{00} + \ket{11}}{\sqrt 2},\quad
  \frac{\ket{00} - \ket{11}}{\sqrt 2},\quad
  \frac{\ket{01} + \ket{10}}{\sqrt 2},\quad
  \frac{\ket{01} - \ket{10}}{\sqrt 2}
  $$
\end{frame}

\begin{frame}[fragile]{Schr\"{o}dinger Equation}
  \begin{block}{Quantum Mechanics Postulate 5}
    The state of a quantum system \emph{evolves} in time. Denote by
    $\ket{\psi_t}$ the state of the system after an elapsed time of $t$. This
    time evolution satisfies the \alert{Time-dependent Schr\"{o}dinger
    equation} $$i\hbar \pdv{}{t}\ket{\psi_t} = \hat H \ket {\psi_t}$$ where
    $\hat H$, called the \alert{Hamiltonian} of the system, is the
    \emph{energy} observable.
  \end{block}
\end{frame}

\begin{frame}[fragile]{Unitary Dynamics}
  The Schr\"{o}dinger equation is \emph{linear}.
  This means that given a time $t$, the map $\mathcal U_t$ sending $\ket{\psi_0}$ to
  $\ket{\psi_t}$ is linear, and is therefore an operator.

  \pause Moreover, this operator leaves the lengths of vectors unchanged.
  We call operators such as these \alert{unitary} operators.

  \pause Unitary operators will later be used to implement \alert{quantum gates},
  the quantum analog of classical logic gates.
\end{frame}
\begin{frame}[fragile]{Unitary Matrices}
  An operator is unitary if and only if its adjoint is equal to its inverse,
  $$\mathcal U^\dag = \mathcal U^{-1}$$

  \pause Or, in terms of the matrix components of $\mathcal U$, if
  $$\conj{\mathcal U_{nm}} = {\mathcal U}^{-1}_{mn}$$

\end{frame}

\begin{frame}[fragile]{Distinguishability of States}
  Given that a system is in \emph{one} of the states $\{\ket {\psi_i}\}_i$, can
  we determine with certainty which state it is in by performing a
  measurement?\pause

  We can, \emph{if and only if} the states $\ket{\psi_i}$ are orthogonal.\pause

  We do this by applying the unique unitary transformation that sends
  $\ket{\psi_i}$ to $\ket{e_i}$, and then measuring in the $\ket{e_i}$ basis.
\end{frame}

\section{Quantum Computation Theory}

\begin{frame}[fragile]{Qubits}
  Consider a two dimensional Hilbert space with basis vectors $\ket 0$ and $\ket 1$.
  A state $\ket\psi$ of this system is called a \alert{qubit}. The basis vectors
  $\ket 0$ and $\ket 1$ are called the \alert{computational basis states}.

  \pause A qubit can have not only the value 0 and 1, but also any \emph{superposition}
  of 0 and 1,
  $$\ket\psi = \alpha \ket 0 + \beta \ket 1$$
  where $\abs{\alpha}^2 + \abs{\beta}^2 = 1$.
\end{frame}

\begin{frame}[fragile]{Bloch Sphere}
  Because a quantum state is only unique up to a phase, we can in general write
  the state of a qubit as

  $$\ket\psi = \cos\left(\frac \theta 2\right)\ket 0 + e^{i\phi}\sin\left(\frac \theta 2\right)\ket 1$$

  where $0 \leq \theta \leq \pi$ and $0 \leq \phi \leq 2\pi$.

  \pause This can be visualized using the \alert{Bloch sphere}.
\end{frame}
{%
  \setbeamertemplate{frame footer}{https://commons.wikimedia.org/wiki/File:Bloch\_Sphere.svg}
\begin{frame}[fragile]{Bloch Sphere}
  \begin{center}
    \begin{figure}
      \caption{The Bloch Sphere}
      \hspace*{0.8cm}\includegraphics[width=0.5\textheight,height=0.7\textheight,keepaspectratio]{bloch_sphere.png}
    \end{figure}
  \end{center}
\end{frame}
}
\begin{frame}[fragile]{Quantum Gates}
  A \alert{quantum gate} is a unitary transformation on one or a tensor product
  of qubits. This transformation can be represented as a $2^n \times 2^n$
  unitary matrix.\pause

  Quantum gates can be combined together to form a \alert{quantum
  circuit}, in analogy to classical gates and classical logic circuits.\pause

  Unlike classical logic gates, quantum gates must be
  \alert{reversible}. There must be a one-to-one map from the gate's inputs to
  its outputs.
\end{frame}

\begin{frame}[fragile]{One Qubit Gates}
  The only non trivial one-bit classical logic gate is the \emph{not} gate.
  There are, however, many non trivial one qubit gates.

  \pause Each one qubit quantum gate corresponds to a series of rotations of the
  Bloch sphere.
\end{frame}

\begin{frame}[fragile]{Hadamard Gate}
  The \alert{Hadamard gate} $\Qcircuit @C=1em @R=.7em { & \gate{H} & \qw }$ is the quantum gate sending
      $$\ket 0 \to \frac{\ket 0 + \ket 1}{\sqrt2},\qquad
      \ket 1 \to \frac{\ket 0 - \ket 1}{\sqrt2}$$
  The Hadamard gate rotates $\ket\psi$ 180 degrees about the x-axis,
  and then 90 degrees about the y axis.
      \begin{columns}
        \begin{column}{0.33\textwidth}
          \begin{figure}
            \[\frac 1 {\sqrt 2}
            \begin{bmatrix}
              1 & 1\\1 & -1
            \end{bmatrix}
            \hphantom{\frac 1 {\sqrt 2}}\]
            \caption*{Matrix components}
          \end{figure}
        \end{column}
        \begin{column}{0.33\textwidth}
          \begin{figure}
            \scalebox{.70}{\input{bloch_hadamard.pdf_tex}}
            \caption*{Visualization}
          \end{figure}
        \end{column}
      \end{columns}
\end{frame}

\begin{frame}[fragile]{Pauli-X Gate}
  The \alert{Pauli-X gate} $\Qcircuit @C=1em @R=.7em { & \gate{X} & \qw }$ is the quantum gate sending
      $$\ket 0 \to \ket 1,\qquad
        \ket 1 \to \ket 0$$
        The Pauli-X gate is the quantum analog of the classical not gate. It rotates the Bloch sphere
        180 degrees about the x-axis.
      \begin{columns}
        \begin{column}{0.33\textwidth}
          \begin{figure}
            \[
            \begin{bmatrix}
              0 & 1\\1 & 0
            \end{bmatrix}\]
            \caption*{Matrix components}
          \end{figure}
        \end{column}
        \begin{column}{0.33\textwidth}
          \begin{figure}
            \scalebox{.70}{\input{bloch_x.pdf_tex}}
            \caption*{Visualization}
          \end{figure}
        \end{column}
      \end{columns}
\end{frame}

\begin{frame}[fragile]{Pauli-Y Gate}
  The \alert{Pauli-Y gate} $\Qcircuit @C=1em @R=.7em { & \gate{Y} & \qw }$ is the quantum gate sending
      $$\ket 0 \to i\ket 1,\qquad
        \ket 1 \to -i\ket 0$$
       The Pauli-Y gate corresponds to a rotation of the Bloch sphere 180 degrees about the y-axis.
      \begin{columns}
        \begin{column}{0.33\textwidth}
          \begin{figure}
            \[\begin{bmatrix}
              0 & -i\\i & 0
            \end{bmatrix}\]
            \caption*{Matrix components}
          \end{figure}
        \end{column}
        \begin{column}{0.33\textwidth}
          \begin{figure}
            \scalebox{.70}{\input{bloch_y.pdf_tex}}
            \caption*{Visualization}
          \end{figure}
        \end{column}
      \end{columns}
\end{frame}

\begin{frame}[fragile]{Pauli-Z Gate}
  The \alert{Pauli-Z gate} $\Qcircuit @C=1em @R=.7em { & \gate{Z} & \qw }$ is the quantum gate sending
      $$\ket 0 \to \ket 0,\qquad
        \ket 1 \to -\ket 1$$
     The Pauli-Z gate corresponds to a rotation of the Bloch sphere 180 degrees about the z-axis.
      \begin{columns}
        \begin{column}{0.33\textwidth}
          \begin{figure}
            \[\begin{bmatrix}
              1 & 0\\0 & -1
            \end{bmatrix}\]
            \caption*{Matrix components}
          \end{figure}
        \end{column}
        \begin{column}{0.33\textwidth}
          \begin{figure}
            \scalebox{.70}{\input{bloch_z.pdf_tex}}
            \caption*{Visualization}
          \end{figure}
        \end{column}
      \end{columns}
\end{frame}

\begin{frame}[fragile]{CNOT Gate}
  The \alert{CNOT gate}, or \alert{controlled not gate},
  is a \emph{two qubit} gate. It sends
  the basis vector $\ket{ij}$ to $\ket{i(i \oplus j)}$.

  \bigskip
  \begin{columns}
    \begin{column}{0.5\textwidth}
      $$\begin{bmatrix}
        1 & 0 & 0 & 0\\
        0 & 1 & 0 & 0\\
        0 & 0 & 0 & 1\\
        0 & 0 & 1 & 0\\
      \end{bmatrix}$$
    \end{column}
    \begin{column}{0.5\textwidth}
      \Qcircuit @C=1em @R=.7em @! {
        \lstick{\ket i} & \ctrl{1} & \rstick{\ket i} \qw \\
        \lstick{\ket j} & \targ & \rstick{\ket{i \oplus j}} \qw
      }
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Universal Gates}
  Any quantum circuit can be realized by combining CNOT gates with single qubit gates.
  These gates are said to be \alert{universal} for quantum computation.
\end{frame}

\begin{frame}[fragile]{Toffoli Gate}
  The \alert{Toffoli gate} is a \emph{three} qubit gate that sends the
  vector $\ket{ijk}$ to $\ket{ij(k \oplus ij)}$

  \[
    \Qcircuit @C=.7em @R=.8em @! {
      \lstick{\ket i} & \ctrl{2} & \rstick{\ket i} \qw\\
        \lstick{\ket j} & \ctrl{1} & \rstick{\ket j} \qw\\
        \lstick{\ket k} & \targ & \rstick{\ket{k \oplus ij}} \qw
      }
    \]

  \medskip
  Using \alert{ancillary qubits}, the Toffoli Gate can be used to implement
  \emph{any} classical circuit with quantum gates.
\end{frame}

\section{Applications}

\begin{frame}[fragile]{Create EPR Pair}
  Create a pair of entangled qubits.

  \[
    \Qcircuit @C=.7em @R=.4em @! {
      \lstick{\ket 0} & \gate{H} & \ctrl{1} & \qw \\
      \lstick{\ket 0} & \qw      & \targ & \qw
    }
  \]\pause

  The output of this circuit is $$\frac{\ket{00} + \ket{11}}{\sqrt 2}$$
  More generally, it maps the computational basis to the Bell basis.
\end{frame}

\begin{frame}[fragile]{Distinguish Bell States}
  Distinguish between Bell states.

  \[
    \Qcircuit @C=.7em @R=.4em @! {
      \lstick{} & \ctrl{1} & \gate{H} & \meter \qw & \rstick{i} \cw \\
      \lstick{} & \targ    & \qw      & \meter \qw & \rstick{j} \cw
      \inputgroupv{1}{2}{.8em}{.8em}{\ket{\beta_{ij}}}
      \gategroup{1}{2}{2}{3}{1.0em}{--}
    }
  \]\pause

  The action of this circuit is given by
  $$\ket{00}\to\frac{\ket{00} + \ket{10}}{\sqrt 2}\qquad
    \ket{01}\to\frac{\ket{01} + \ket{11}}{\sqrt 2}$$
  $$\ket{10}\to\frac{\ket{01} - \ket{11}}{\sqrt 2}\qquad
    \ket{11}\to\frac{\ket{00} - \ket{10}}{\sqrt 2}$$

  which maps the Bell states to the computational basis states.
\end{frame}

\begin{frame}[fragile]{Super Dense Coding}
  Alice uses quantum entanglement to send \emph{two} classical bits
  to Bob using just \emph{one} qubit.

  \[
    \Qcircuit @C=.7em @R=.4em @! {
                      &          &          & \ustick{b_0}   & \ustick{b_1}    &              &          & \\
      \lstick{\ket 0} & \gate{H} & \ctrl{1} & \gate{X} \cwx & \gate{Z} \cwx & \ctrl{1} & \gate{H} & \meter & \rstick{b_0} \cw \\
      \lstick{\ket 0} & \qw      & \targ    & \qw      & \qw & \targ        & \qw      & \meter & \rstick{b_1} \cw
      \gategroup{2}{6}{3}{8}{1.0em}{--}
    }
  \]
\end{frame}

\begin{frame}[fragile]{Quantum Teleportation}
  Alice uses quantum entanglement to send \emph{one} qubit to bob,
  just by transmitting \emph{two} classical bits.

  \[
    \Qcircuit @C=.7em @R=.4em @! {
      \lstick{\ket{\psi}} & \qw & \qw & \ctrl{1} & \gate{H} & \meter & \control \cw\\
      \lstick{\ket{0}} & \qw & \targ & \targ & \qw & \meter & \cwx\\
      \lstick{\ket{0}} & \gate{H} & \ctrl{-1} & \qw & \qw & \gate{X} \cwx & \gate{Z} \cwx & \rstick{\ket{\psi}} \qw
      \gategroup{3}{4}{3}{7}{1.0em}{--}
    }
  \]
\end{frame}
{%
  \setbeamercolor{palette primary}{fg=black, bg=yellow}
  \begin{frame}[standout]
    Questions?
  \end{frame}
}%
\appendix

\begin{frame}[allowframebreaks]{References}
  \bibliography{demo}
  \bibliographystyle{abbrv}
\end{frame}

\end{document}
