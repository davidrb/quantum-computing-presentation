# basic principles of quantum mechanics
- states
  - hilbert space
    - vectors
    - inner product
    - complete inner product space
  - orthonormal basis
    - components of vector
  - dual vectors
  - bra/ket notation
- observables
  - linear operator
  - eigenvector/value
  - adjoint of operator
  - hermitian operators
    - real eigenvalues
    - ON basis of eigenvectors
- measurement
  - superposition
  - probability amplitudes
  - "wave function"
    - "collapse"
  - electron spin example
    - stern-gerlach apparatus

- entanglement
  - tensor product
  - bell states

- schrodinger equation
  - hamiltonian
  - e.o.m. of quantum system
    - netwton: v' = v + F/m dt, x' = x + vdt
    - schrodinger: |psi'> = |psi> + hbar/i H |psi>
  - time evolution operator
    - schrodinger equation is linear
    - time-independent hamiltonian: U = exp(-itH)
    - unitary operators

# quantum computing
- qubits
  - bloch sphere
- quantum circuits
  - quantum gates
    - reversibility
    - basic types of gates
    - universal gates
  - classical circuits with quantum gates

# applications of quantum computing
- quantum teleportation
- super dense coding
  - maximum amount of information sent with 1 qubit
    - without entanglement = 1
    - with entanglement = 2
- quantum fourier transform
