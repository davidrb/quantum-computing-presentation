#!/usr/bin/python
import numpy as np
import math as m

np.set_printoptions(linewidth=100)

def lg(n):
    return int(round(m.log(n, 2)))

def print_ket(ket):
    n = lg(len(ket))
    for i in range(len(ket)):
        print("%+.2f" % (ket.item(i)) + "|" + ("{0:0" + str(n) + "b}").format(i) + "> ", end='')
    print('')

def deutsch_josza(f):
    n = lg(len(f))
    H = 2**(-1/2)*np.matrix('1 1; 1 -1')

    HN = H
    for _ in range(lg(n)):
        HN = np.kron(HN, H);

    HNinv = np.linalg.inv(HN);

    O = np.diag(list(map(lambda x: (-1)**x, f)))

    # initial state
    x0 = np.zeros(shape=(len(f), 1))
    x0[0][0] = 1

    x1 = HN*x0 # apply hadamard gates
    x2 = O*x1  # apply oracle gate
    x3 = HNinv*x2 # apply inverse Hadamard gates

    print('f =', f, '\n');

    print('matrix for quantum circuit:')
    print(HNinv*O*HN, '\n')

    print('x1 = ', end='');
    print_ket(x1)
    print('x2 = ', end='');
    print_ket(x2)
    print('x3 = ', end='');
    print_ket(x3)

    if (np.isclose(x3[0][0], 1)):
        print('zero')
    else:
        print('balanced')

deutsch_josza([0, 0, 0, 0])
print('')
deutsch_josza([0, 1, 1, 0])
#print('')
#deutsch_josza([0, 1, 0, 1, 1, 1, 0, 0])
#print('')
#deutsch_josza([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
#print('')
#deutsch_josza([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1])
#print('')
#deutsch_josza([1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0])
